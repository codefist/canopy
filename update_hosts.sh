#!/usr/bin/env bash

ETC_HOSTS=/etc/hosts

IP="127.0.0.1"
DOMAIN="your-domain.local"

SUBDOMAINS=( react-frontend )

function add_subdomain() {
  SUBDOMAIN=$1
  HOSTNAME="$SUBDOMAIN.$DOMAIN"
  HOSTS_LINE="$IP $HOSTNAME"
  if [ -n "$(grep $HOSTNAME $ETC_HOSTS)" ]
    then
      echo "$HOSTNAME already exists : $(grep $HOSTNAME $ETC_HOSTS)"
    else
      echo "Adding $HOSTNAME to your $ETC_HOSTS";
      echo $HOSTS_LINE >> $ETC_HOSTS;

      if [ -n "$(grep $HOSTNAME $ETC_HOSTS)" ]
        then
          echo "$HOSTS_LINE was added succesfully";
        else
          echo "Failed to Add $HOSTNAME, Try again!";
      fi
  fi
}

for sub_domain in "${SUBDOMAINS[@]}"
do
  add_subdomain $sub_domain
done


echo "----------------------------"
echo "#######  /etc/hosts  #######"
echo "----------------------------"
cat /etc/hosts
