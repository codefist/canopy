# Welcome to the Canopy pattern

### What is this?!
Honestly it's just a docker compose config, with HAProxy registered as a service, and a sane and simple (and non-invasive) way of plugging in your own services.

### What do you get?!
The ability to easily develop microservices.  Yeah, I said microservices and easy in one sentence.

### Sounds legit, but how do I use it?!
Just follow the pattern.  In this repo I include a demo React app, a postgres service, and a demo backend to connect the two.  By leveraging this pattern you could easily be developing a service that depends on one or two or dozens or hundreds of other services - and all the dependencies could be running locally within the same virtual network that Docker Compose provides for free.  Think big...

### But my junior and mid-level guys aren't smart enough?!
Nay!  The "we'll never find devs who can ..." is a myth, and a poisonous mindset that needs to be forgotten.  All the talk in the industry about code refactoring... when microservice architecture is the same thing, just one level of abstraction higher.  You think you're cool for extracting a method into it's own Class?  Maybe... But if you can extract an entire area of your codebase into a new service, you ARE cool (according to the author).  In other words, microservice architecture isn't hard, and shouldn't be treated as such; it's only another layer of abstraction.
